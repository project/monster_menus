(function ($, Drupal, drupalSettings) {
  $.extend(Drupal.theme, {
    tableDragChangedMarker: function tableDragChangedMarker() {
      return "";
    },
    tableDragChangedWarning: function tableDragChangedWarning() {
      return "";
    },
    tableDragToggle: function tableDragToggle() {
      return "";
    }
  });

  $.extend(Drupal.tableDrag.prototype.row.prototype, {
    markChanged: function () {
      var serialStr = '';
      $("#manage-bookmarks-table .tb-manage-name").each(function () {
        serialStr += $(this).attr("name") + "|";
      });
      $.post(Drupal.mmBrowserAppendParams(Drupal.url("mm-bookmarks/sort")),
        {neworder: serialStr.substring(0, serialStr.length - 1)},
        function () {
          $('#manage-bookmarks-table tr').removeClass('drag-previous');
          Drupal.mmBrowserGetBookmarks();
          $("<div class=\"tabledrag-changed-warning messages messages--warning\">" + Drupal.t('Changes have been saved.') + "</div>")
            .insertAfter('#manage-bookmarks-table')
            .fadeOut(3000, function() { $(this).remove(); });
        },
        "json");
    }
  });

  Drupal.behaviors.tableDrag.attach(document, drupalSettings);

  var resetBookmark = function(mmtid, title) {
    $(".bookmark_tr_" + mmtid)
      .filter(':visible')
        .remove()
      .end()
      .find('div.tb-manage-name')
        .html(title)
      .end()
      .show();
    return false;
  };

  $.extend(Drupal, {
    mmBrowserDeleteBookmarkConfirm: function (mmtid, obj) {
      if (confirm(Drupal.t('Are you sure you want to DELETE @title?', {'@title': $(obj).closest('tr').find('div.tb-manage-name').text().trim()}))) {
        return Drupal.mmBrowserDeleteBookmark(mmtid, document);
      }
      return false;
    },

    mmBrowserEditBookmarkEdit: function (event) {
      obj = $(event.target);
      var mmtid = obj.attr('name');
      var title = $('div.tb-manage-name', obj.parent()).text().trim();
      event.stopPropagation();
      var row = $(".bookmark_tr_" + mmtid);
      row
        .clone()
          .html('<td class="tb-manage-name"><form action="#" onsubmit="Drupal.mmBrowserSaveBookmark(' + mmtid + '); return false;"><input type="text" maxlength="35" name="edittitle-' + mmtid + '" value="' + title.replace(/"/g, '&quot;') + '"><input type="hidden" name="editmmtid-' + mmtid + '" value="' + mmtid + '"></form></td><td><a href="#" onclick="return Drupal.mmBrowserSaveBookmark(' + mmtid + ');">' + Drupal.t('Save') + '</a></td><td><a href="#" name="bookmark-cancel">' + Drupal.t('Cancel') + '</a></td>')
          .insertAfter(row)
          .find('[name=bookmark-cancel]').click(function () {
            return resetBookmark(mmtid, title);
          })
          .end()
          .find('[name=edittitle-' + mmtid + ']')
            .focus()
          .end()
        .end().end()
        .hide();
    },

    mmBrowserDeleteBookmark: function (mmtid, context) {
      $.post(
        Drupal.mmBrowserAppendParams(drupalSettings.path.baseUrl + "mm-bookmarks/delete/" + mmtid),
        {},
        function () {
          $(".bookmark_tr_" + mmtid, context).remove();
          Drupal.mmBrowserGetBookmarks();
        },
        "json"
      );
      return false;
    },

    mmBrowserSaveBookmark: function (emmtid) {
      var mmtid = $("input[name=editmmtid-" + emmtid + "]").val();
      $.post(
        Drupal.mmBrowserAppendParams(drupalSettings.path.baseUrl + "mm-bookmarks/edit/" + mmtid),
        {title: $("input[name=edittitle-" + emmtid + "]").val()},
        function (data) {
          resetBookmark(data.mmtid, data.title);
          Drupal.mmBrowserGetBookmarks();
        },
        "json"
      );
      return false;
    }
  });

})(jQuery, Drupal, drupalSettings);