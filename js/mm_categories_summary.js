(function ($, Drupal, drupalSettings) {
  drupalSettings.MM.summaryFuncs['edit-mm-categories'] = function (context) {
    return Drupal.formatPlural($('div.mm-list,div.mm-list-selected', context).length + (typeof drupalSettings.MM.categories_summary !== 'undefined' ? drupalSettings.MM.categories_summary.offset : 0), '1 page', '@count pages');
  };
})(jQuery, Drupal, drupalSettings);
