(function ($, Drupal) {
  Drupal.behaviors.MMTooltip = {
    attach: function (context) {
      $('.tooltip-link', context).once('mm-tooltip').each(function () {
        $(this).tooltip({
          content: function () {
            return this.title;
          }
        });
      })
    }
  }
})(jQuery, Drupal);