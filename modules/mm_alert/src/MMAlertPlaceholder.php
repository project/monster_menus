<?php

namespace Drupal\mm_alert;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\filter\Render\FilteredMarkup;
use Drupal\monster_menus\Constants;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

class MMAlertPlaceholder implements TrustedCallbackInterface {

  /** @var AccountInterface */
  protected $currentUser;

  /** @var EntityTypeManagerInterface */
  protected $typeManager;

  /** @var Connection */
  protected $database;

  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $type_manager, Connection $database) {
    $this->currentUser = $current_user;
    $this->typeManager = $type_manager;
    $this->database = $database;
  }

  /**
   * @inheritDoc
   */
  public static function trustedCallbacks() {
    return ['getPlaceholder'];
  }

  public function getPlaceholder() {
    $frequency = mm_get_setting('nodes.alert_frequency');

    $last = 0;
    if ($frequency == 'once') {
      $last = $this->currentUser->getLastAccessedTime();
    }
    elseif ($frequency == 'login') {
      if (!empty($_SESSION['mm_node_displayed_alert'])) {
        return [];
      }
      $_SESSION['mm_node_displayed_alert'] = TRUE;
    }

    $output = [
      '#cache' => ['max-age' => -1],
    ];
    $select = $this->database->select('node_field_data', 'n');
    $select->join('mm_node_schedule', 's', 's.nid = n.nid');
    $select->addField('n', 'nid');
    $select->addExpression('IF(s.set_change_date = 1 AND s.publish_on > 0, s.publish_on, n.changed)', 'sort_column');
    $select->condition('n.status', 1)
      ->condition('n.type', 'alert')
      ->condition('s.publish_on', mm_request_time(), '<=')
      ->condition((new Condition('OR'))
        ->condition('s.unpublish_on', 0)
        ->condition('s.unpublish_on', mm_request_time(), '>')
      )
      ->condition((new Condition('OR'))
        ->where(':last = 0', array(':last' => $last))
        ->where(':last BETWEEN s.publish_on AND n.changed - 1', array(':last' => $last))
        ->condition('s.publish_on', $last, '>')
      );
    $select->orderBy('n.sticky', 'DESC');
    $select->orderBy('sort_column', 'DESC');
    if (!empty($_SESSION['mm_node_alert_avoid_nid'])) {
      $select->condition('n.nid', $_SESSION['mm_node_alert_avoid_nid'], 'NOT IN');
    }

    $nids = $select->execute()->fetchCol();
    mm_ui_modal_dialog([], $output);
    $output['#attached']['library'][] = 'mm_alert/mm_alert';
    $output['#cache']['tags'] = Cache::buildTags('node', $nids);
    $count_displayed = 0;

    /** @var Node $node */
    foreach (Node::loadMultiple($nids) as $node) {
      if (mm_content_user_can_node($node, Constants::MM_PERMS_READ) && !mm_content_node_is_recycled($node, Constants::MM_NODE_RECYCLED_MMTID_CURR)) {
        $output[] = [
          '#prefix' => FilteredMarkup::create('<div class="hidden mm-alert" id="mm-alert-' . $node->id() . '">'),
          $this->typeManager->getViewBuilder('node')->view($node),
          '#suffix' => FilteredMarkup::create('<div class="mm-alert-close"><input type="button" value="' . t('Close') . '" /></div></div>'),
        ];
        $count_displayed++;
      }
    }
    if ($count_displayed && $frequency == 'once') {
      // Prevent this alert from showing again.
      User::load($this->currentUser->id())->setLastAccessTime(mm_request_time())->save();
    }

    return $count_displayed ? $output : [];

  }

}