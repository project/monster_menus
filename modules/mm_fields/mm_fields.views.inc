<?php

use Drupal\field\FieldStorageConfigInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;

/**
 * Implements hook_field_views_data().
 */
function mm_fields_field_views_data(FieldStorageConfigInterface $field) {
  $data = views_field_default_views_data($field);
  foreach ($data as $table_name => $table_data) {
    foreach ($table_data as $field_name => $field_data) {
      if (isset($field_data['filter']) && $field_name != 'delta') {
        // Filter: swap the handler to the 'in' operator.
        $data[$table_name][$field_name]['filter']['id'] = 'many_to_one';
      }
    }

    if ($field->getType() == 'mm_catlist') {
      // Relationship: add a relationship for related MM page.
      $field_name = $field->getName() . '_' . $field->getMainPropertyName();
      list($label) = views_entity_field_label('node', $field->getName());
      $data[$table_name][$field_name . '_contents']['relationship'] = array(
        'base' => 'node_field_data',
        'base field' => 'nid',
        'label' => t('Page contents referred to by @field.', ['@field' => $label]),
        'id' => 'sequential_join',
        'joins' => [
          [
            'left table' => $table_name,
            'left field' => $field_name,
            'table' => 'mm_node2tree',
            'field' => 'mmtid',
          ],
          [
            'left field' => 'nid',
            'table' => 'node_field_data',
            'field' => 'nid',
          ],
        ],
      );
    }
  }
  return $data;
}

/**
 * Implements hook_field_views_data_views_data_alter().
 *
 * Views integration to provide reverse relationships on mm_catlist fields.
 */
function mm_fields_field_views_data_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {
  if ($field_storage->getType() != 'mm_catlist') {
    return;
  }
  $entity_type_id = $field_storage->getTargetEntityTypeId();
  $field_name = $field_storage->getName();
  $entity_type_manager = \Drupal::entityTypeManager();
  $entity_type = $entity_type_manager->getDefinition($entity_type_id);
  $pseudo_field_name = 'reverse_' . $field_name . '_' . $entity_type_id;
  /** @var DefaultTableMapping $table_mapping */
  $table_mapping = $entity_type_manager->getStorage($entity_type_id)->getTableMapping();
  list($label) = views_entity_field_label($entity_type_id, $field_name);

  $data['node_field_data'][$pseudo_field_name]['relationship'] = [
    'title' => t('@entity using @field', ['@entity' => $entity_type->getLabel(), '@field' => $label]),
    'label' => t('@field_name', ['@field_name' => $field_name]),
    'help' => t('Relate page contents to a @field field.', ['@field' => $label]),
    'group' => $entity_type->getLabel(),
    'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
    'entity_type' => $entity_type_id,
//    'base field' => $entity_type->getKey('id'),
    'id' => 'sequential_join',
    'joins' => [
      [
        'left table' => 'node_field_data',
        'left field' => 'nid',
        'table' => 'mm_node2tree',
        'field' => 'nid',
      ],
      [
        'left field' => 'mmtid',
        'table' => $table_mapping->getDedicatedDataTableName($field_storage),
        'field' => $field_name . '_' . $field_storage->getMainPropertyName(),
      ],
      [
        'left field' => 'entity_id',
        'table' => 'node_field_data',
        'field' => 'nid',
      ],
    ],
  ];
}
