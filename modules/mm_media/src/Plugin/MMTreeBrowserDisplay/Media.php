<?php

namespace Drupal\mm_media\Plugin\MMTreeBrowserDisplay;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\mm_media\Plugin\EntityBrowser\Widget\MMTree;
use Drupal\monster_menus\Constants;
use Drupal\monster_menus\MMTreeBrowserDisplay\MMTreeBrowserDisplayInterface;
use Drupal\monster_menus\Plugin\MMTreeBrowserDisplay\Fallback;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides the MM Tree display generator for pages containing Media entities.
 *
 * @MMTreeBrowserDisplay(
 *   id = "mm_tree_browser_display_media",
 *   admin_label = @Translation("MM Tree media display"),
 * )
 */
class Media extends Fallback implements MMTreeBrowserDisplayInterface {

  const BROWSER_MODE_MEDIA = 'med';

  /**
   * The database connection.
   *
   * @var Connection
   */
  protected $database;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The HTTP query.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $httpQuery;

  /**
   * If set, allow the implicit creation of Media entities from File entities.
   *
   * @var bool
   */
  protected $allow_file2media = FALSE;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->entityFieldManager = \Drupal::service('entity_field.manager');
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->httpQuery = \Drupal::request()->query;
    $this->database = Database::getConnection();
  }

  /**
   * @inheritDoc
   */
  public static function supportedModes() {
    return [self::BROWSER_MODE_MEDIA];
  }

  /**
   * @inheritDoc
   */
  public function label($mode) {
    return t('Select a file upload');
  }

  /**
   * {@inheritdoc}
   */
  public function alterLeftQuery($mode, $query, &$params) {
    $segments = [];
    $types_join = '';

    $in = [];
    foreach ($this->getFieldsForActiveFileTypes() as $i => $field) {
      $table = 'media__' . $field;
      $in[] = "m$i.{$field}_target_id";
      $types_join .= " LEFT JOIN $table m$i ON m$i.entity_id = %alias.mid";
    }

    if (!$in) {
      throw new BadRequestHttpException(t('No active media types were found.'));
    }

    $types_join .= ' INNER JOIN {file_managed} fm ON fm.fid IN(' . implode(', ', $in) . ')';

    foreach ($this->getFields() as $alias => $fields_list) {
      foreach ($fields_list as $fields) {
        $join = [];
        foreach ($fields as $field) {
          $join[] = '{' . $field[0] . '} ' . $field[1] . ' ON ' . $field[2];
        }
        $joined = join(' INNER JOIN ', $join);
        if (!strncmp($alias, 'file_managed', 12)) {
          $this_types_join = '';
          $concat_alias = $field[1];
        }
        else {
          $this_types_join = str_replace('%alias', $alias, $types_join);
          $concat_alias = 'fm';
        }
        $segments[] = "SELECT GROUP_CONCAT($concat_alias.fid) FROM {node_field_data} n INNER JOIN $joined INNER JOIN {mm_node2tree} n2 ON n2.nid = n.nid" . $this_types_join . ' WHERE n.status = 1 AND n2.mmtid = o.container';
      }
    }

    // Ideally, we would use a UNION to get all the distinct fids, but can't
    // because then MySQL doesn't let us use o.container from the outer query.
    // Instead, get a concatenated list of all the mids in nodecount and
    // squash the duplicates in PHP to get the count.
    $params[Constants::MM_GET_TREE_ADD_SELECT] = "CONCAT_WS(',', (" . join('), (', $segments) . ')) AS fid_list';
    $params[Constants::MM_GET_TREE_FILTER_NORMAL] = $params[Constants::MM_GET_TREE_FILTER_USERS] = TRUE;
  }

  /**
   * @return array
   */
  private function getFields() {
    $output = [];
    $file_types = $this->getFileTypes();
    $paragraph_data = [];

    foreach ($this->entityFieldManager->getActiveFieldStorageDefinitions('paragraph') as $field_name => $field_def) {
      $type = 1;
      if ($field_def->getType() == 'entity_reference' && $field_def->getSetting('target_type') == 'media' ||
          $this->allow_file2media && in_array($field_def->getType(), $file_types) && !$field_def->hasCustomStorage() && ($type = 2)) {
        foreach ($this->entityFieldManager->getFieldMap()['paragraph'][$field_name]['bundles'] ?? [] as $paragraph) {
          $ids = $this->entityTypeManager->getStorage('paragraph')->getQuery()
            ->condition('type', $paragraph)
            ->execute();
          foreach (Paragraph::loadMultiple($ids) as $loaded) {
            if ($loaded->get('parent_type')->value == 'node') {
              $paragraph_data[$loaded->get('parent_field_name')->value] = [$field_name, $type];
            }
          }
        }
      }
    }

    foreach ($this->entityFieldManager->getActiveFieldStorageDefinitions('node') as $field_name => $field_def) {
      // Perform a dummy entity query, in order to have it map the table
      // relationships.
      $query = \Drupal::entityQuery('node');
      if ($field_def->getType() == 'entity_reference' && $field_def->getSetting('target_type') == 'media') {
        $query->condition("$field_name.entity.bundle", 1);
      }
      else if ($this->allow_file2media && in_array($field_def->getType(), $file_types) && !$field_def->hasCustomStorage()) {
        $query->condition("$field_name.entity", 1);
      }
      else if (isset($paragraph_data[$field_name])) {
        $suffix = $paragraph_data[$field_name][1] == 1 ? '.entity.bundle' : '.entity';
        $query->condition("$field_name.entity:paragraph." . $paragraph_data[$field_name][0] . $suffix, 1);
      }
      else {
        continue;
      }

      // Sadly, entityQuery doesn't have public methods for most things, so hack
      // it.
      // Start with $query->prepare().
      $prepare = new \ReflectionMethod(get_class($query), 'prepare');
      $prepare->setAccessible(TRUE);
      $prepare->invoke($query);
      // $query->compile().
      $compile = new \ReflectionMethod(get_class($query), 'compile');
      $compile->setAccessible(TRUE);
      $compile->invoke($query);
      // Get $query->sqlQuery.
      $rp = new \ReflectionProperty(get_class($query), 'sqlQuery');
      $rp->setAccessible(TRUE);
      $sqlQuery = $rp->getValue($query);

      $join = [];
      $last_alias = '';
      foreach ($sqlQuery->getTables() as $table_name => $table_def) {
        if ($table_name != 'base_table' && $table_name != 'media_field_data') {
          if ($join && preg_match('{ \[?base_table\]?\.}', $table_def['condition'])) {
            $output[$last_alias][] = $join;
            $join = [];
          }
          $last_alias = $table_def['alias'];
          $join[] = [
            $table_def['table'],
            $table_def['alias'],
            preg_replace('{ \[?base_table\]?\.}', ' n.', $table_def['condition']),
          ];
        }
      }
      $output[$last_alias][] = $join;
    }

    return $output;
  }

  /**
   * Sanitize and expand the browserFileTypes query parameter.
   *
   * @return array
   */
  private function getFileTypes() {
    if ($types = $this->httpQuery->get('browserFileTypes')) {
      $types = array_map(function($mime) {
        return preg_replace('/[^A-Za-z0-9_.]+/', '', $mime);
      }, explode(',', $types));
      if (($i = array_search(MMTree::FILE2MEDIA_TYPE, $types)) !== FALSE) {
        unset($types[$i]);
        $this->allow_file2media = TRUE;
      }
      return $types;
    }
    return [];
  }

  private function getFieldsForActiveFileTypes() {
    $out = [];
    if (!($file_types = $this->getFileTypes())) {
      throw new BadRequestHttpException(t('No media types are associated with this field.'));
    }
    $types = $this->entityTypeManager->getStorage('media_type')->loadMultiple($file_types);
    foreach ($types as $type) {
      $field = $type->getPluginCollections()['source_configuration']->getConfiguration()['source_field'];
      if ($field != 'field_media_oembed_video') {
        $out[] = $field;
      }
    }
    return $out;
  }

  /**
   * @inheritDoc
   */
  public function showReservedEntries($mode) {
    return FALSE;
  }

  /**
   * Get a list of file upload thumbnails for the right hand column.
   *
   * {@inheritdoc}
   */
  public function viewRight($mode, $params, $perms, $item, $database) {
    $mmtid = $item->mmtid;
    if (!$perms[Constants::MM_PERMS_APPLY]) {
      $out = '';
      if ($mmtid > 0) {
        $out = '<div id="mmtree-browse-thumbnails"><br />' . t('You do not have permission to use the file uploads on this page.') . '</div>';
      }
      $json = array(
        'title' => mm_content_get_name($mmtid),
        'body' => $out,
      );
      return mm_json_response($json);
    }

    $fm_segments = FALSE;
    foreach ($this->getFields() as $fields_list) {
      foreach ($fields_list as $fields) {
        $segment = $this->database->select('node_field_data', 'n');
        foreach ($fields as $i => $field) {
          if (!$i) {
            $segment->addTag(__FUNCTION__ . '__' . $field[0] . '.' . $field[1]);
          }
          $segment->join($field[0], $field[1], $field[2]);
          if ($field[0] == 'media') {
            $segment->addExpression($field[1] . '.mid', 'mid');
            $segment->addExpression(-1, 'fid');
          }
          else if ($field[0] == 'file_managed') {
            $segment->addExpression(-1, 'mid');
            $segment->addExpression($field[1] . '.fid', 'fid');
            $fm_segments = TRUE;
          }
        }
        $segment->join('mm_node2tree', 'n2', 'n2.nid = n.nid');
        $segment->condition('n.status', 1, '=')
          ->condition('n2.mmtid', $mmtid, '=');

        if (empty($query)) {
          $query = $segment;
        }
        else {
          $query->union($segment);
        }
      }
    }

    $content = [];
    if (!empty($query)) {
      $query = $this->database->select($query, 'subquery');
      $query->addTag(__FUNCTION__);
      if ($mode == self::BROWSER_MODE_MEDIA) {
        $query->addField('subquery', 'mid');
      }

      $in = [];
      foreach ($this->getFieldsForActiveFileTypes() as $i => $field) {
        $table = 'media__' . $field;
        $in[] = "m$i.{$field}_target_id";
        $query->leftJoin($table, "m$i", "m$i.entity_id = subquery.mid");
      }
      if ($fm_segments) {
        $in[] = 'subquery.fid';
      }

      if (!$in) {
        return [];
      }

      $query->join('file_managed', 'fm', 'fm.fid IN(' . implode(', ', $in) . ')');
      $query->fields('fm');
      $result = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->orderBy('changed', 'DESC')
        ->limit(mm_get_setting('nodes.nodelist_pager_limit'))
        ->execute();

      $min_wh[0] = $this->httpQuery->getInt('browserMinW', 0);
      $min_wh[1] = $this->httpQuery->getInt('browserMinH', 0);

      foreach ($result as $file) {
        $content['icons'][] = [
          '#theme' => 'mm_browser_thumbnail',
          '#file' => $file,
          '#style_name' => 'thumbnail',
          '#mode' => $mode,
          '#mmtid' => $mmtid,
          '#min_wh' => $min_wh,
        ];
      }
    }

    if (!$content) {
      $content = [['#prefix' => '<p>', '#markup' => t('There is no selectable content on this page.'), '#suffix' => '</p>']];
    }
    else {
      $content['pager'] = ['#type' => 'pager'];
    }

    return ['#prefix' => '<div id="mmtree-browse-thumbnails">', $content, '#suffix' => '</div>'];
  }

}
