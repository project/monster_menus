<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform\WebformEntityElementsForm;

class MMWebformEntityElementsForm extends WebformEntityElementsForm {

  use MMWebformFixEntityTrait;

}
