<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Url;
use Drupal\webform\Ajax\WebformRefreshCommand;
use Drupal\webform\WebformEntityHandlersForm;
use Drupal\webform\WebformInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MMWebformEntityHandlersForm extends WebformEntityHandlersForm {

  use MMWebformFixEntityTrait;

  /**
   * Calls a method on a webform handler and reloads the webform handlers form.
   *
   * @param string $webform_handler
   *   THe webform handler id.
   * @param string $operation
   *   The operation to perform, e.g., 'enable' or 'disable'.
   * @param Request $request
   *   The current request.
   *
   * @return AjaxResponse|RedirectResponse
   *   Either returns an AJAX response that refreshes the webform's handlers
   *   page, or redirects back to the webform's handlers page.
   */
  public static function mmAjaxOperation($webform_handler, $operation, Request $request) {
    /** @var WebformInterface $webform */
    if (!$webform = static::getWebformFromRoute()) {
      throw new NotFoundHttpException();
    }
    parent::ajaxOperation($webform, $webform_handler, $operation, $request);

    $params = \Drupal::routeMatch()->getParameters();
    $url = (new Url('mm_webform.handlers', [
        'mm_tree' => $params->get('mm_tree')->id(),
        'node' => $params->get('node')->id(),
      ],
      [
        'query' => ['update' => $webform_handler],
        'absolute' => TRUE,
      ]
    ))->toString();

    // If the request is via AJAX, return the webform handlers form.
    if ($request->request->get('js')) {
      $response = new AjaxResponse();
      $response->addCommand(new WebformRefreshCommand($url));
      return $response;
    }

    // Otherwise, redirect back to the webform handlers form.
    return new RedirectResponse($url);
  }

}
