<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform\EntitySettings\WebformEntitySettingsConfirmationForm;

class MMWebformEntitySettingsConfirmationForm extends WebformEntitySettingsConfirmationForm {

  use MMWebformFixEntityTrait;

}
