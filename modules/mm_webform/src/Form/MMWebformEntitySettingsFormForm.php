<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform\EntitySettings\WebformEntitySettingsFormForm;

class MMWebformEntitySettingsFormForm extends WebformEntitySettingsFormForm {

  use MMWebformFixEntityTrait;

}
