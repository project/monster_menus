<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform\EntitySettings\WebformEntitySettingsGeneralForm;

class MMWebformEntitySettingsGeneralForm extends WebformEntitySettingsGeneralForm {

  use MMWebformFixEntityTrait;

}
