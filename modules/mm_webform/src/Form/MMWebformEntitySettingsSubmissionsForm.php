<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform\EntitySettings\WebformEntitySettingsSubmissionsForm;

class MMWebformEntitySettingsSubmissionsForm extends WebformEntitySettingsSubmissionsForm {

  use MMWebformFixEntityTrait;

}
