<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface;

/**
 * Trait MMWebformFixElementTrait
 *
 * Override the buildForm() method that is part of various Webform UI
 * element form handlers, providing $webform.
 */
trait MMWebformFixElementTrait {

  use MMWebformGetWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL) {
    if ($webform = static::getWebformFromRoute()) {
      return parent::buildForm($form, $form_state, $webform);
    }
  }

}
