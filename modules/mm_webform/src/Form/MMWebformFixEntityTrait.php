<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Trait MMWebformFixEntityTrait
 *
 * Override the buildForm() method that is part of various Webform
 * configuration form handlers, providing $this->entity in cases where it would
 * not otherwise be present.
 */
trait MMWebformFixEntityTrait {

  use MMWebformGetWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (empty($this->entity->id())) {
      if ($webform = static::getWebformFromRoute()) {
        $this->setEntity($webform);
      }
    }
    return parent::buildForm($form, $form_state);
  }

}
