<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform\Entity\Webform;

trait MMWebformGetWebformTrait {

  /**
   * Find the Webform associated with the current request, if any.
   *
   * @return Webform|null
   */
  public static function getWebformFromRoute() {
    if ($node = \Drupal::routeMatch()->getParameters()->get('node')) {
      if ($field_def = mm_webform_get_webform_field($node)) {
        $field_name = $field_def->getName();
        if (isset($node->get($field_name)->getValue()[0]['target_id'])) {
          $target_id = $node->get($field_name)->getValue()[0]['target_id'];
          /** @var Webform $webform */
          if ($webform = Webform::load($target_id)) {
            return $webform;
          }
        }
      }
    }
    return NULL;
  }

}
