<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Form\WebformHandlerAddForm;
use Drupal\webform\WebformInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MMWebformHandlerAddForm extends WebformHandlerAddForm {

  use MMWebformGetWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, $webform_handler = NULL) {
    if ($webform = static::getWebformFromRoute()) {
      return parent::buildForm($form, $form_state, $webform, $webform_handler);
    }
    // Throw access denied if handler is excluded.
    if (!empty($this->webformHandler) && $this->webformHandler->isExcluded()) {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $params = \Drupal::routeMatch()->getParameters();
    $form_state->setRedirect('mm_webform.handlers', [
        'mm_tree' => $params->get('mm_tree')->id(),
        'node' => $params->get('node')->id(),
      ],
      ['query' => ['update' => $this->webformHandler->getHandlerId()]]
    );
  }

}
