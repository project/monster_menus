<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Form\WebformHandlerDeleteForm;
use Drupal\webform\WebformInterface;

class MMWebformHandlerDeleteForm extends WebformHandlerDeleteForm {

  use MMWebformGetWebformTrait;
  use MMWebformSubmitHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, $webform_handler = NULL) {
    if ($webform = static::getWebformFromRoute()) {
      return parent::buildForm($form, $form_state, $webform, $webform_handler);
    }
  }

}
