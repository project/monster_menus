<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;

trait MMWebformSubmitElementTrait {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $params = \Drupal::routeMatch()->getParameters();
    $form_state->setRedirect('mm_webform.edit_form', [
      'mm_tree' => $params->get('mm_tree')->id(),
      'node' => $params->get('node')->id(),
    ]);
  }

}
