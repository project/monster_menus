<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform_ui\Form\WebformUiElementAddForm;

class MMWebformUiElementAddForm extends WebformUiElementAddForm {

  use MMWebformGetWebformTrait;
  use MMWebformSubmitElementTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, $key = NULL, $parent_key = NULL, $type = NULL) {
    $webform = $webform ?? static::getWebformFromRoute();
    return parent::buildForm($form, $form_state, $webform, $key, $parent_key, $type);
  }

}
