<?php

namespace Drupal\mm_webform\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform_ui\Form\WebformUiElementEditForm;

class MMWebformUiElementEditForm extends WebformUiElementEditForm {

  use MMWebformSubmitElementTrait;
  use MMWebformGetWebformTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, $key = NULL, $parent_key = NULL, $type = NULL) {
    if (empty($webform)) {
      $webform = static::getWebformFromRoute();
    }
    return parent::buildForm($form, $form_state, $webform, $key, $parent_key, $type);
  }

}
