<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform_ui\Form\WebformUiElementTypeChangeForm;

class MMWebformUiElementTypeChangeForm extends WebformUiElementTypeChangeForm {

  use MMWebformFixEntityTrait;

}
