<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform_ui\Form\WebformUiElementTypeSelectForm;

class MMWebformUiElementTypeSelectForm extends WebformUiElementTypeSelectForm {

  use MMWebformFixElementTrait;

}
