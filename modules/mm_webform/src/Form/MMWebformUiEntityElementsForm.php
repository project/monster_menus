<?php

namespace Drupal\mm_webform\Form;

use Drupal\webform_ui\WebformUiEntityElementsForm;

class MMWebformUiEntityElementsForm extends WebformUiEntityElementsForm {

  use MMWebformFixEntityTrait;

}
