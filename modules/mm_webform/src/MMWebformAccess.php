<?php

namespace Drupal\mm_webform;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\webform\Access\WebformHandlerAccess;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformInterface;
use Drupal\webform_ui\Access\WebformUiAccess;

class MMWebformAccess {

  public static function getWebformForNode(NodeInterface $node) {
    if ($field_def = mm_webform_get_webform_field($node)) {
      $field_name = $field_def->getName();
      if (isset($node->get($field_name)->getValue()[0]['target_id'])) {
        $target_id = $node->get($field_name)->getValue()[0]['target_id'];
        /** @var WebformInterface $webform */
        if ($webform = Webform::load($target_id)) {
          return $webform;
        }
      }
    }
  }

  public static function access(AccountInterface $account, NodeInterface $node) {
    if ($field_def = mm_webform_get_webform_field($node)) {
      $field_name = $field_def->getName();
      return AccessResult::allowedIf(isset($node->get($field_name)->getValue()[0]['target_id']) && $node->access('update', $account));
    }
    return AccessResult::forbidden();
  }

  public static function accessViewSubmission(AccountInterface $account, NodeInterface $node) {
    return static::accessSubmission($account, $node, 'view');
  }

  public static function accessSubmission(AccountInterface $account, NodeInterface $node, $op) {
    if ($field_def = mm_webform_get_webform_field($node)) {
      $field_name = $field_def->getName();
      /** @var WebformInterface $webform */
      if (isset($node->get($field_name)->getValue()[0]['target_id']) && $node->access('view', $account) && ($webform = Webform::load($node->get($field_name)->getValue()[0]['target_id']))) {
        return mm_webform_webform_submission_access(NULL, $op, $account, $webform);
      }
    }
    return AccessResult::forbidden();
  }

  public static function accessWebformElement($type, NodeInterface $node, AccountInterface $account) {
    if (class_exists('Drupal\webform_ui\Access\WebformUiAccess') && ($webform = static::getWebformForNode($node))) {
      return WebformUiAccess::checkWebformElementAccess($webform, $type, $account);
    }
    return AccessResult::forbidden();
  }

  public static function accessWebformEdit(NodeInterface $node, AccountInterface $account) {
    if (class_exists('Drupal\webform_ui\Access\WebformUiAccess') && ($webform = static::getWebformForNode($node))) {
      return WebformUiAccess::checkWebformEditAccess($webform, $account);
    }
    return AccessResult::forbidden();
  }

  public static function accessHandler(NodeInterface $node, $webform_handler = NULL) {
    if ($webform = static::getWebformForNode($node)) {
      return WebformHandlerAccess::checkHandlerAccess($webform_handler);
    }
    return AccessResult::forbidden();
  }

}
