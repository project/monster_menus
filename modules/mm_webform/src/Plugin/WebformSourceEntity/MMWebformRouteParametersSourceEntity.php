<?php

namespace Drupal\mm_webform\Plugin\WebformSourceEntity;

use Drupal\webform\Plugin\WebformSourceEntity\RouteParametersWebformSourceEntity;

/**
 * Detect source entity by examining route parameters. As this is a replacement
 * for RouteParametersWebformSourceEntity, there should be no annotation here.
 */
class MMWebformRouteParametersSourceEntity extends RouteParametersWebformSourceEntity {

  /**
   * {@inheritdoc}
   */
  public function getSourceEntity(array $ignored_types) {
    // Don't return a source entity when the user is editing the elements of an
    // existing webform, as this causes a WebformException later on.
    if (in_array($this->routeMatch->getRouteName(), ['mm_webform_ui.element.edit_form', 'mm_webform_ui.element.add_page', 'mm_webform_ui.element.add_layout', 'mm_webform_ui.element.add_form', 'mm_webform_ui.element.duplicate_form'])) {
      return NULL;
    }
    return parent::getSourceEntity($ignored_types);
  }

}
