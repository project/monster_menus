<?php

namespace Drupal\mm_webform\RouteProcessor;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\Routing\Route;

/**
 * Processes outbound routes, switching them to node-specific routes when needed.
 */
class OutboundRouteProcessor implements OutboundRouteProcessorInterface {

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Constructs a OutboundRouteProcessor object.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   */
  public function __construct(RouteProviderInterface $route_provider) {
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($route_name, Route $route, array &$parameters, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (!($node = \Drupal::service('current_route_match')->getParameters()->get('node')) || !mm_webform_has_webform_field($node)) {
      return;
    }
    $routes = [
      'entity.webform.handler.edit_form' =>         'mm_webform.handler.edit_form',
      'entity.webform.handler.duplicate_form' =>    'mm_webform.handler.duplicate_form',
      'entity.webform.handler.delete_form' =>       'mm_webform.handler.delete_form',
      'entity.webform.handler.disable' =>           'mm_webform.handler.disable',
      'entity.webform.handler.enable' =>            'mm_webform.handler.enable',
      'entity.webform_ui.element' =>                'mm_webform_ui.element',
      'entity.webform_ui.element.add_form' =>       'mm_webform_ui.element.add_form',
      'entity.webform_ui.element.add_page' =>       'mm_webform_ui.element.add_page',
      'entity.webform_ui.element.add_layout' =>     'mm_webform_ui.element.add_layout',
      'entity.webform_ui.element.edit_form' =>      'mm_webform_ui.element.edit_form',
      'entity.webform_ui.element.duplicate_form' => 'mm_webform_ui.element.duplicate_form',
      'entity.webform_ui.element.delete_form' =>    'mm_webform_ui.element.delete_form',
    ];
    if (isset($routes[$route_name])) {
      $source_route = $this->routeProvider->getRouteByName($routes[$route_name]);
      $route->setPath($source_route->getPath());
      $route->setDefaults($source_route->getDefaults());
      $route->setRequirements($source_route->getRequirements());
      $route->setOptions($source_route->getOptions());
      $route->setHost($source_route->getHost());
      $route->setSchemes($source_route->getSchemes());
      $route->setMethods($source_route->getMethods());
      foreach (['mm_tree', 'node'] as $parameter_name) {
        if ($parameter = \Drupal::request()->attributes->get($parameter_name)) {
          $parameters[$parameter_name] = is_numeric($parameter) ? $parameter : $parameter->id();
        }
      }
    }
  }

}
