<?php

namespace Drupal\mm_workflow_access\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks access to Workflow tab.
 */
class WorkflowHistoryAccess implements AccessInterface {

  private $access;

  /**
   * Check if the user has permissions to view the Workflow tab.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user account.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Current routeMatch.
   * @param \Symfony\Component\Routing\Route $route
   *   Current route.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   If the user can access this workflow.
   */
  public function access(AccountInterface $account, RouteMatchInterface $routeMatch, Route $route) {
    $uid = $account ? $account->id() : -1;
    $entity = workflow_url_get_entity(NULL, $routeMatch);
    $entity_id = $entity->id();
    $entity_type = $entity->getEntityTypeId();
    $entity_bundle = $entity->bundle();
    $route_name_array = explode('.', $routeMatch->getRouteName());
    $field_name = $route_name_array[3] ?? 'no_field';

    if (!isset($this->access[$uid][$entity_type][$entity_id][$field_name])) {
      $access_result = AccessResult::forbidden();

      // Prohibit access if there are no workflow fields on this node.
      $fields = _workflow_info_fields($entity, $entity_type, $entity_bundle, $route_name_array[3] ?? '');
      if ($fields) {
        if (workflow_node_current_state($entity)) {
          if ($account->hasPermission('bypass node access') || $account->hasPermission('administer nodes')) {
            $access_result = AccessResult::allowed();
          }
          else {
            foreach ($fields as $definition) {
              $type_id = $definition->getSetting('workflow_type');
              // Preserve the misspelling of "trasition" in the permission names.
              if ($account->hasPermission("access any $type_id workflow_transion overview")) {
                $access_result = AccessResult::allowed();
              }
              elseif (!empty($entity->workflow_author) && $entity->workflow_author == $entity->getOwnerId() && $account->hasPermission("access own $type_id workflow_transion overview")) {
                $access_result = AccessResult::allowed();
              }
            }
          }
        }
      }

      $this->access[$uid][$entity_type][$entity_id][$field_name] = $access_result;
    }

    return $this->access[$uid][$entity_type][$entity_id][$field_name];
  }

}