<?php

namespace Drupal\mm_workflow_access\Routing;

use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Workflow routes.
 */
class RouteSubscriber extends \Drupal\workflow\Routing\RouteSubscriber {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $field_list = workflow_get_workflow_fields_by_entity_type();
    foreach ($field_list as $entityTypeId => $fields) {
      if ($entityTypeId == 'node') {
        if ($route = $collection->get("entity.node.workflow_history")) {
          $route->setRequirement('_custom_access', '\Drupal\mm_workflow_access\Access\WorkflowHistoryAccess::access');
        }

        foreach ($fields as $field_name => $field) {
          if ($route = $collection->get("entity.node.workflow_history.$field_name")) {
            $route->setRequirement('_custom_access', '\Drupal\mm_workflow_access\Access\WorkflowHistoryAccess::access');
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 99];
    return $events;
  }

}
