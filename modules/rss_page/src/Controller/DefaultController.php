<?php
/**
 * @file
 * Contains \Drupal\rss_page\Controller\DefaultController.
 */

namespace Drupal\rss_page\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default controller for the rss_page module.
 */
class DefaultController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var Connection
   */
  protected $database;

  /**
   * The service container.
   *
   * @var ContainerInterface
   */
  protected $container;

  /**
   * Constructs a DefaultController object.
   *
   * @param ContainerInterface $container
   *   The service container.
   * @param Connection $database
   *   The database connection.
   */
  public function __construct(ContainerInterface $container, Connection $database) {
    $this->container = $container;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('database')
    );
  }

  /**
   * Retrieve a string of taxonomy autocomplete suggestions
   */
  public function taxonomyAutocomplete(Request $request) {
    $string = trim($request->query->get('q'));
    $matches = [];
    if ($string != '') {
      $result = $this->database->queryRange('SELECT t.tid, t.name FROM {taxonomy_term_field_data} t WHERE t.name LIKE :string', 0, 10, array(':string' => "%$string%"));

      foreach ($result as $tag) {
        $label = Html::escape($tag->name);
        $matches[] = ['value' => $label . ' (' . $tag->tid . ')', 'label' => $label];
      }
    }

    return mm_json_response($matches);
  }

}
