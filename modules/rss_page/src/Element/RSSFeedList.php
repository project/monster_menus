<?php
/**
 * @file
 * Contains \Drupal\rss_page\Element\RSSFeedList.
 */

namespace Drupal\rss_page\Element;

use Drupal\Core\Url;
use Drupal\monster_menus\Element\MMCatlist;
use Drupal\monster_menus\Element\MMRepeatlist;

/**
 * Provides a form element which allows the user to manipulate a list of RSS
 * feeds.
 *
 * @FormElement("rss_feed_list")
 */
class RSSFeedList extends MMRepeatlist {

  public function getInfo() {
    $class = get_class($this);
    return [
      '#input'                      => TRUE,
      '#default_value'              => [],
      '#process'                    => [[$class, 'processGroup']],
      '#pre_render'                 => [[$class, 'preRenderGroup'],
                                        [$class, 'preRender']],
      '#attached'                   => ['library' => ['monster_menus/mm', 'monster_menus/modal_dialog']],
      '#rss_list_url_form_ID'       => '',
      '#rss_list_tax_form_ID'       => '',
      '#rss_list_query_form_ID'     => '',
      '#rss_list_is_portal_page'    => FALSE,
      '#theme'                      => 'rss_feed_list',
      '#theme_wrappers'             => ['form_element'],
    ];
  }

  /**
   * Add Javascript code to a page, allowing the user to manipulate a list of
   * RSS feeds.
   *
   * @param array $element
   *   The form element to display
   * @return array
   *   The modified form element
   */
  public static function preRender($element) {
    if (isset($element['#mm_list_instance'])) {
      return $element;
    }
    $_mmlist_instance = &drupal_static('_mmlist_instance', 0);
    $labelAddURL = t('Add RSS feed...');
    if (!$element['#rss_list_is_portal_page']) {
      $is_portal = FALSE;
      $flags = array();
      $labelAboveInfo = t('Location:');
      $labelAddQuery = '';
    }
    else {
      $is_portal = TRUE;
      $flags = array('is_portal' => TRUE);
      $labelAboveInfo = '';
      $labelAddQuery = t('Add a query...');
    }
    $labelAboveList = t('Feeds:');
    $labelAboveActions = t('Action:');
    $labelAddCat = t('Add page feed...');
    $labels = mm_ui_mmlist_labels();

    $delConfirm = t("Are you sure you want to delete this feed?\n\n(You can skip this alert in the future by holding the Shift key while clicking the Delete icon.)");

    $popup_base = Url::fromRoute('monster_menus.browser_load', [], ['query' => ['_path' => "1-rss-$_mmlist_instance-r-r/"]])->toString();
    $popup_URL = mm_home_mmtid();

    $adds = array();
    if (is_string($element['#value']) && substr($element['#value'], 0, 2) === 'a:') {
      foreach (unserialize($element['#value']) as $obj) {
        $url = '';
        self::getAdd($obj->type, $obj->data, $obj->name, $url, $info, $popup_URL, $is_portal);
        $adds[] = [$obj->name, $obj->type, $url, $info];
      }
    }
    else {
      foreach (_rss_page_split_feed_list($element['#value']) as $m) {
        $name = $m->name;
        $url = !$is_portal && $m->type == 'cat' ? '' : $m->data;
        self::getAdd($m->type, $m->data, $name, $url, $info, $popup_URL, $is_portal);
        $adds[] = [$name, $m->type, $url, $info];
      }
    }

    if (!isset($popup_URL)) {
      $popup_URL = 1;
    }
    $popup_label = t('Select a page');

    $imgpath = base_path() . \Drupal::service('extension.list.module')->getPath('monster_menus') . '/images';

    $url_form = $element['#rss_list_url_form_ID'];
    $tax_form = $element['#rss_list_tax_form_ID'];
    $query_form = $element['#rss_list_query_form_ID'];
    $labelAddTax = t('Add tag feed...');

    $name = $element['#parents'][0];
    if (count($element['#parents']) > 1) {
      $name .= '[' . join('][', array_slice($element['#parents'], 1)) . ']';
    }

    $labelAboveList .= !empty($element['#required']) ? ' <span class="form-required" title="' . t('This field is required.') . '">*</span>' : '';
    $class = MMCatlist::addClass($element);

    $settings = [
      'where'              => NULL,
      'isSearch'           => mm_ui_is_search(),
      'listObjDivSelector' => "div[name=rss_list_obj$_mmlist_instance]",
      'outerDivSelector'   => "div[name=rss_list_obj$_mmlist_instance] + div[class=\"$class\"]",
      'hiddenName'         => $name,
      'add'                => $adds,
      'autoName'           => NULL,
      'parms'              => [
        'popupBase'         => $popup_base,
        'popupURL'          => $popup_URL,
        'popupLabel'        => $popup_label,
        'flags'             => $flags,
        'addCallback'       => 'rssAddCallback',
        'replaceCallback'   => 'rssReplCallback',
        'selectCallback'    => 'rssSelectCallback',
        'dataCallback'      => 'rssDataCallback',
        'labelAboveActions' => $labelAboveActions,
        'labelAddCat'       => $labelAddCat,
        'labelAddURL'       => $labelAddURL,
        'labelAddQuery'     => $labelAddQuery,
        'labelAddList'      => $labelAddTax,
        'labelAboveInfo'    => $labelAboveInfo,
        'imgPath'           => $imgpath,
        'inputDivID'        => $url_form,
        'inputDivID2'       => $tax_form,
        'inputDivID3'       => $query_form,
        'delConfirmMsg'     => $delConfirm,
        'labelTop'          => $labels[0],
        'labelUp'           => $labels[1],
        'labelX'            => $labels[2],
        'labelBott'         => $labels[3],
        'labelDown'         => $labels[4],
        'labelEdit'         => $labels[5],
      ],
    ];
    mm_ui_modal_dialog('init', $element);
    $element['#attached']['drupalSettings']['MM']['mmListInit'][$_mmlist_instance] = $settings;
    $element += [
      '#mm_list_instance' => $_mmlist_instance++,
      '#mm_list_class' => $class,
      '#label_above_list' => $labelAboveList,
//      '#mm_list_desc' => !empty($element['#description']) ? $element['#description'] : '',
    ];
    return $element;
  }

  private static function getAdd($type, $data, &$name, &$url, &$info, &$popup_URL, $is_portal) {
    if ($type == 'cat') {
      $parents = mm_content_get_parents($data);
      array_shift($parents);  // skip root
      $url = implode('/', $parents) . "/$data";
      if (!isset($popup_URL)) {
        $popup_URL = $url;
      }

      $path = array();
      foreach ($parents as $par) {
        if (!($tree = mm_content_get($par))) {
          break;
        }
        $path[] = mm_content_get_name($tree);
      }

      $path[] = $name;
      $info = implode('&nbsp;&raquo; ', $path);
    }
    elseif ($is_portal && $type == 'query') {
      $info = $name;
      $url = $data;
    }
    elseif ($type == 'taxon') {
      $info = '';
      $url = $data;
    }
    else {  // URL
      $u = preg_replace('/\/(?!\/)/', '/&#8203;', $data);
      $info = strpos($url, str_replace('...', '', $name)) ? $u : $name . '<br />' . $u;
      $url = $data;
    }
  }

}
