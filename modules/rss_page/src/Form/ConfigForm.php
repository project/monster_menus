<?php

/**
 * @file
 * Contains \Drupal\monster_menus\rss_page\Form\ConfigForm.
 */

namespace Drupal\rss_page\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rss_page_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rss_page.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('rss_page.settings');

    $form['cache_path'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('RSS cache path'),
      '#description' => $this->t('The location of the cache used to keep from fetching the same remote RSS data repeatedly.'),
      '#default_value' => $config->get('cache_path'),
      '#weight' => 3,
    );
    $form['cache_duration'] = array(
      '#type' => 'select',
      '#title' => $this->t('RSS cache duration'),
      '#description' => $this->t('After this length of time, cached remote RSS data will be discarded.'),
      '#default_value' => $config->get('cache_duration'),
      '#options' => array(
        30   => t('30 seconds'),
        60   => t('1 minute'),
        300  => t('5 minutes'),
        600  => t('10 minutes'),
        1800 => t('30 minutes'),
        3600 => t('1 hour'),
        7200 => t('2 hours'),
      ),
      '#weight' => 4,
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('rss_page.settings')
      ->set('cache_path', $values['cache_path'])
      ->set('cache_duration', $values['cache_duration'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
