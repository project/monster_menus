<?php

namespace Drupal\rss_page\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\monster_menus\Constants;
use Drupal\monster_menus\Plugin\Block\MMTreeBlock;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

class RSSPageBase extends MMTreeBlock {

  /** @var array If set, only these cache tags will be applied to the output. */
  public $cacheTagMask = [];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'delta' => 'content',
      'field_rss_page_items' => 10,
      'field_rss_page_options' => ['show_descr' => TRUE, 'show_feed_img' => TRUE, 'show_image' => TRUE],
      'field_rss_page_sort' => 'feed-weight',
      'rss_feeds' => [],
      'user_configurable' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $delta = isset($config['delta']) && in_array($config['delta'], ['content', 'nav']) ? $config['delta'] : 'content';
    // Start with the base block.
    $new_tags = ['rss_page:' . $this->configuration['id'] . ':' . $delta];
    if (isset($this->configuration['key'])) {
      // Add tag, if this is a derivative.
      $new_tags[] = 'rss_page:' . $this->configuration['key'] . ':' . $delta;
    }
    $tags = Cache::mergeTags(parent::getCacheTags(), $new_tags);

    if (!empty($this->configuration['rss_feeds'])) {
      // Add tags for feeds.
      $feed_tags = [];
      foreach (_rss_page_split_feed_list($this->configuration['rss_feeds']) as $feed) {
        switch ($feed->type) {
          case 'taxon':
            if (preg_match('/\((\d+)\)$/', $feed->data, $matches)) {
              $feed_tags[] = 'taxonomy_term:' . $matches[1];
            }
            break;
          case 'cat':
            $feed_tags[] = 'mm_tree:' . $feed->data;
            break;
          case 'query':
          case 'url':
            break;
        }
      }
      $tags = Cache::mergeTags($tags, $feed_tags);
    }

    if ($this->cacheTagMask) {
      // Mask-off any tags we don't care about.
      $masked = [];
      foreach ($tags as $tag) {
        foreach ($this->cacheTagMask as $mask) {
          if (preg_match("/^$mask:/", $tag)) {
            $masked[] = $tag;
            break;
          }
        }
      }
      return $masked;
    }

    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    /** @var NodeInterface $node */
    if (!isset($config['node']) || !($node = Node::load($config['node']))) {
      $node = Node::create($config + ['type' => 'rss_page', 'title' => $config['label']]);
    }
    $edit = $config['edit'] ?? [];
    $delta = isset($config['delta']) && in_array($config['delta'], ['content', 'nav']) ? $config['delta'] : 'content';
    $block = _rss_page_block($delta, $node, $rss_link, FALSE, $edit);
    if ($node && !empty($node->label())) {
      $block['#subject'] = Html::escape(_rss_page_add_vars($node->label(), $node));
    }
    if ($node && !$node->isNew() && mm_content_user_can_node($node, Constants::MM_PERMS_WRITE)) {
      $block['content'][] = array('#weight' => -1, '#markup' => '<div class="portal-page-setup">' . Link::fromTextAndUrl(t('setup'), Url::fromRoute('entity.node.canonical', ['node' => $node->id()]))->toString() . '</div>');
    }
    $weight = 0;
    foreach (Element::children($block['content']) as $index) {
      if (!isset($block['content'][$index]['#weight'])) {
        $block['content'][$index]['#weight'] = $weight++;
      }
    }
    // Allow this block to be hidden in homebox_amherst.
    if (!$block['#count'] && isset($edit['homebox']) && empty($edit['homebox']->closable)) {
      $block['content'] = array();
    }
    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    _rss_page_main_form($short_node_form, $form_state, $config['rss_feeds']);
    $node = Node::create(['type' => 'rss_page']);
    /** @var EntityFormBuilder $entity_form_builder */
    $entity_form_builder = \Drupal::getContainer()->get('entity.form_builder');
    $long_node_form = $entity_form_builder->getForm($node);
    $form += [
      '#tree' => FALSE,
      'rss_settings' => [
        '#type' => 'details',
        '#title' => $long_node_form['group_rss_page_display']['#title'],
        '#open' => TRUE,
        'field_rss_page_items' => [
          '#title' => $long_node_form['field_rss_page_items']['widget'][0]['value']['#title'],
          '#type' => 'select',
          '#options' => $long_node_form['field_rss_page_items']['widget'][0]['value']['#options'],
          '#default_value' => $config['field_rss_page_items'],
        ],
        'field_rss_page_options' => [
          '#type' => 'checkboxes',
          '#options' => $long_node_form['field_rss_page_options']['widget']['#options'],
          '#default_value' => $config['field_rss_page_options'],
        ],
        'field_rss_page_sort' => [
          '#type' => 'radios',
          '#title' => $long_node_form['field_rss_page_sort']['widget']['#title'],
          '#options' => $long_node_form['field_rss_page_sort']['widget']['#options'],
          '#default_value' => $config['field_rss_page_sort'],
        ],
        '#weight' => 1,
      ],
    ];
    foreach (['rss_feeds', 'feeds-tax', 'feeds-url'] as $field) {
      $form[$field] = $short_node_form[$field];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    foreach (['label', 'label_display', 'provider', 'field_rss_page_items', 'field_rss_page_options', 'field_rss_page_sort', 'rss_feeds'] as $key) {
      $this->configuration[$key] = method_exists($form_state, 'getCompleteFormState') ? $form_state->getCompleteFormState()->getValue($key) : $form_state->getValue($key);
    }
    Cache::invalidateTags($this->getCacheTags());
  }

}
