<?php

namespace Drupal\rss_page\Plugin\Block;

/**
 * Provides an RSS Page content block.
 *
 * @Block(
 *   id = "rss_page_content_block",
 *   admin_label = @Translation("RSS Page content"),
 * )
 */
class RSSPageContent extends RSSPageBase {
}
