<?php

namespace Drupal\rss_page\Plugin\Block;

/**
 * Provides an RSS Page navigation block.
 *
 * @Block(
 *   id = "rss_page_navigation_block",
 *   admin_label = @Translation("RSS Page navigation"),
 * )
 */
class RSSPageNavigation extends RSSPageBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['delta' => 'nav'] + parent::defaultConfiguration();
  }

}
