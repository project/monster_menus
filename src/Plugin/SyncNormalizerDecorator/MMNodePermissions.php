<?php

namespace Drupal\monster_menus\Plugin\SyncNormalizerDecorator;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides a decorator for copying node permissions to/from an exported entity.
 *
 * @SyncNormalizerDecorator(
 *   id = "mm_node_permissions",
 *   name = @Translation("Copy MM node permissions"),
 * )
 */
class MMNodePermissions extends SyncNormalizerDecoratorBase {

  /**
   * @inheritDoc
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
    if ($entity->getEntityTypeId() == 'node') {
      $normalized_entity['groups_w'] = $entity->groups_w;
      $normalized_entity['users_w'] = $entity->users_w;
      $normalized_entity['others_w'] = [$entity->others_w];
    }
  }

  /**
   * @inheritDoc
   */
  public function decorateDenormalizedEntity(ContentEntityInterface $entity, array $normalized_entity, $format, array $context = []) {
    if ($entity->getEntityTypeId() == 'node') {
      $entity->groups_w = $normalized_entity['groups_w'];
      $entity->users_w = $normalized_entity['users_w'];
      $entity->others_w = $normalized_entity['others_w'][0];
    }
  }

}
